﻿language_guanche = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_guanche
		}
	}
	parameters = {
		language_group_berber = yes
		language_family_afro_asiatic = yes 		
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_guanche }
			multiply = 10
		}
		else_if = {
			limit = { has_cultural_parameter = language_group_berber }
			multiply = 5
		}
		else_if = {
			limit = { has_cultural_parameter = language_family_afro_asiatic }
			multiply = 2.5
		}
	}
	
	color = guanche
}